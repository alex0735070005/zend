<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;
use Application\Controller\Factory\AppControllerFactory;
use Application\Controller\HomeController;
use Application\Controller\LoginController;
use Application\Controller\RegistrationController;
use Application\Controller\NewsController;

use Zend\Mvc\Controller\LazyControllerAbstractFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\HomeController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\HomeController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            
            'newsShow' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news[/:slug]',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'show',
                    ],
                ],
            ],
            'newsAdd' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news/add',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'add',
                    ],
                ],
            ],
            'newsEdit' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news/edit[/:slug]',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'edit',
                    ],
                ],
            ],
            'newsDelete' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news/delete[/:slug]',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'delete',
                    ],
                ],
            ],
            'newsList' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news/list',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'list',
                    ],
                ],
            ],
        ],
    ],
    
    'controllers' => [
        'factories' => [
            HomeController::class           => AppControllerFactory::class, 
            LoginController::class          => AppControllerFactory::class,
            RegistrationController::class   => AppControllerFactory::class,
            NewsController::class           => AppControllerFactory::class
        ],
    ],
    
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'                     => __DIR__ . '/../view/layout/layout.phtml',
            'application/home/index'            => __DIR__ . '/../view/application/home/index.phtml',
            'application/news/add'              => __DIR__ . '/../view/application/news/add.phtml',
            'application/news/show'             => __DIR__ . '/../view/application/news/show.phtml',
            'application/news/edit'             => __DIR__ . '/../view/application/news/edit.phtml',
            'application/news/delete'           => __DIR__ . '/../view/application/news/delete.phtml',
            'application/news/list'             => __DIR__ . '/../view/application/news/list.phtml',
            'error/404'                         => __DIR__ . '/../view/error/404.phtml',
            'error/index'                       => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    

    'doctrine' => array(
        'driver' => array(
            'Application_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                     'Application\Entity' =>  'Application_driver'
                ),
            ),
        ),
    ),                 

];
