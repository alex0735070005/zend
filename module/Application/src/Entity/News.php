<?php

namespace Application\Entity;
 
use Doctrine\ORM\Mapping as ORM;
 
/**
 * User
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="Application\Repository\NewsRepository")
 */

class News
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $date_create;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * Many News have One User.
     * @ORM\ManyToOne(targetEntity="User\Entity\User", inversedBy="news")
     */
    private $user;
    
    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

        
    public function __construct() 
    {
        $this->date_create = new \DateTime('now');
    }
    
    public function getId() {
        return $this->id;
    }

    public function getDateCreate(){
        return $this->date_create->format('d.m.Y');
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDateCreate(datetime $date_create) {
        $this->date_create = $date_create;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
}