<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Application\Entity\News;
use Zend\Authentication\AuthenticationService;

class NewsController extends AbstractActionController
{
    
    private $entityManager;
    private $authService;
  
    
    public function __construct(EntityManager $entityManager, AuthenticationService $authService) 
    {
        $this->entityManager  = $entityManager;
        $this->authService    = $authService;
    }
    
    public function addAction()
    {
       
        $request = $this->getRequest();
        
        if($request->isPost())
        {
            $Repo = $this->entityManager->getRepository(News::class);
           
            $Repo->addNews($request->getPost(), $this->authService->getIdentity());
               
           return $this->redirect()->toRoute('newsList');
        }
        
        return new ViewModel();
    }
    
    public function showAction()
    {
        $id = $this->params()->fromRoute('slug', -1);
                
        $News = $this->entityManager->find(News::class, $id);
        
        return new ViewModel(['News'=> $News]);
    }
    
    public function listAction()
    {
        
        $Repo = $this->entityManager->getRepository(News::class);
           
        $dataNews = $Repo->getListNews($this->authService->getIdentity());
        
        //var_dump($dataNews); die();
        
        return new ViewModel(['news' => $dataNews]);
    }
    
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('slug', -1);                
        $News = $this->entityManager->find(News::class, $id);
       
        $request = $this->getRequest();
        
        if($request->isPost())
        {
            $data = $request->getPost();
            
            $News->setTitle($data['title']);
            $News->setDescription($data['description']);
            $this->entityManager->persist($News);
            $this->entityManager->flush();
        }
        
        return new ViewModel(['News'=> $News]);
    }
    
    public function deleteAction()
    {
        $id = $this->params()->fromRoute('slug', -1);                
        $News = $this->entityManager->find(News::class, $id);
        
        $this->entityManager->remove($News);
        $this->entityManager->flush();
        
        $View = new ViewModel();
        
        $View->setTemplate('application/news/delete.phtml');
        
        return $View;
    }
}
