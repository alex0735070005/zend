<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Entity\User;

class HomeController extends AbstractActionController
{
    
    private $entityManager;
  
    
    public function __construct($entityManager) 
    {
        
        $this->entityManager = $entityManager;
    }
    
    public function indexAction()
    {
        //var_dump($this->entityManager->find(User::class, 1)); die;
        
        
        return new ViewModel();
    }
}
