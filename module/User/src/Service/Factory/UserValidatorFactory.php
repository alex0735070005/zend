<?php
namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Service\UserValidator;

/**
 * This is the factory class for UserValidator service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class UserValidatorFactory
{
    /**
     * This method creates the UserValidator service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {                  
        return new UserValidator();
    }
}
