<?php
namespace User\Service;

class UserValidator {
    
    public function validRegistration($data)
    {
        $errors = [];
        
        if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $errors['email'] = true;
        }
        
        if(!isset($data['full_name']) || !preg_match('/^[a-zA-Z0-9]{4,25}$/', $data['full_name'])){
            $errors['full_name'] = true;
        }
        
        if(!isset($data['password']) || !preg_match('/^[a-zA-Z0-9]{4,25}$/', $data['password'])){
            $errors['password'] = true;
        }
        
        if(!isset($data['confirm_password']) || !preg_match('/^[a-zA-Z0-9]{4,25}$/', $data['confirm_password'])){
            $errors['confirm_password'] = true;
        }
        
        if(isset($data['confirm_password']) && isset($data['password']) && $data['confirm_password'] !== $data['password']){
            $errors['confirm_password'] = true;
        }
        return $errors;
    }
    
    
    public function validLogin($data)
    {
        $errors = [];
        
       if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $errors['email'] = true;
        }
       
        if(!isset($data['password']) || !preg_match('/^[a-zA-Z0-9]{4,25}$/', $data['password'])){
            $errors['password'] = true;
        }
        
        return $errors;
    }
}