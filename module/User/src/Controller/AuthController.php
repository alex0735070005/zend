<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;
use User\Entity\User;

/**
 * This controller is responsible for letting the user to log in and log out.
 */
class AuthController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Auth manager.
     * @var User\Service\AuthManager
     */
    private $authManager;

    /**
     * User manager.
     * @var User\Service\UserManager
     */
    private $userManager;
    
    /**
     * User validator.
     * @var User\Service\UserValidator
     */
    private $userValidator;

    /**
     * Constructor.
     */
    public function __construct($entityManager, $authManager, $userManager, $userValidator)
    {
        $this->entityManager    = $entityManager;
        $this->authManager      = $authManager;
        $this->userManager      = $userManager;
        $this->userValidator    = $userValidator;
    }

    /**
     * Authenticates user given email address and password credentials.
     */
    public function loginAction()
    {
        // Store login status.
        $isErrors = [];

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            
            $isErrors = $this->userValidator->validLogin($data);

            // Validate form
            if(!$isErrors) {

                // Perform login attempt.
                $result = $this->authManager->login($data['email'],
                        $data['password'], 'You login in site');
               
                // Check result.
                if ($result->getCode() == Result::SUCCESS) 
                {
                    return $this->redirect()->toRoute('home');
                }else{
                    $isErrors['not_exist_password_email'] = true;
                }
            }
        }
      
        return new ViewModel([
            'errors' => $isErrors,
        ]);
    }
    
    /**
     * This action displays a page allowing to add a new user.
     */
    public function addAction()
    {
        
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $isErrors = $this->userValidator->validRegistration($data);
            
            if(!$isErrors) {
                
                // Add user.
                if($this->userManager->addUser($data)){
                    // Redirect to "view" page
                    return $this->redirect()->toRoute('login');      
                }else{
                    $isErrors['email_exist'] = true;
                }
            }               
        } 
        
        $View = new ViewModel([
            'errors' => $isErrors,
        ]);
        
        $View->setTemplate('user/auth/registration.phtml');
        
        return $View;
    }

    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction()
    {
        $this->authManager->logout();

        return $this->redirect()->toRoute('login');
    }
}
